<body>
    <div class="preloader">
        <div class="preloader-body">
            <div class="cssload-bell">
                <div class="cssload-circle">
                    <div class="cssload-inner"></div>
                </div>
                <div class="cssload-circle">
                    <div class="cssload-inner"></div>
                </div>
                <div class="cssload-circle">
                    <div class="cssload-inner"></div>
                </div>
                <div class="cssload-circle">
                    <div class="cssload-inner"></div>
                </div>
                <div class="cssload-circle">
                    <div class="cssload-inner"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="page">
        <!-- Page Header-->
        <header class="section page-header header-creative-wrap context-dark">
            <!-- RD Navbar-->
            <div class="rd-navbar-wrap">
                <nav class="rd-navbar rd-navbar-creative rd-navbar-creative-2" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="100px" data-xl-stick-up-offset="112px" data-xxl-stick-up-offset="132px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
                    <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
                    <div class="rd-navbar-aside-outer">
                        <div class="rd-navbar-aside">
                            <div class="rd-navbar-collapse">
                                <ul class="contacts-classic">
                                    <li><span class="contacts-classic-title">Call us:</span> <a href="tel:#">+1 (844) 123 456 78</a>
                                    </li>
                                    <li><a href="mailto:#">info@demolink.org</a></li>
                                </ul>
                            </div>
                            <!-- RD Navbar Panel-->
                            <div class="rd-navbar-panel">
                                <!-- RD Navbar Toggle-->
                                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                                <!-- RD Navbar Brand-->
                                <div class="rd-navbar-brand"><a class="brand" href="index.html"><img class="brand-logo-dark" src="<?= base_url('assets/images/logo-default-120x45.png') ?>" alt="" width="120" height="45" srcset="<?= base_url('assets/images/logo-default-240x90.png') ?> 2x" /><img class="brand-logo-light" src="<?= base_url('assets/images/logo-inverse-119x43.png') ?>" alt="" width="119" height="43" srcset="<?= base_url('assets/images/logo-inverse-238x87.png') ?> 2x" /></a>
                                </div>
                            </div>
                            <div class="rd-navbar-aside-element">
                                <!-- RD Navbar Search-->
                                <div class="rd-navbar-search rd-navbar-search-2">
                                    <button class="rd-navbar-search-toggle rd-navbar-fixed-element-3 rd-navbar-fixed-element-4" data-rd-navbar-toggle=".rd-navbar-search"><span></span></button>
                                    <form class="rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
                                        <div class="form-wrap">
                                            <label class="form-label" for="rd-navbar-search-form-input">Search...</label>
                                            <input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" />
                                            <div class="rd-search-results-live" id="rd-search-results-live"></div>
                                            <button class="rd-search-form-submit fl-bigmug-line-search74" type="submit"></button>
                                        </div>
                                    </form>
                                    <select class="select select-inline rd-navbar-fixed-element-2" data-placeholder="Select an option" data-dropdown-class="select-inline-dropdown">
                                        <option value="en" selected="">en</option>
                                        <option value="fr">fr</option>
                                        <option value="es">es</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rd-navbar-main-outer">
                        <div class="rd-navbar-main">
                            <div class="rd-navbar-nav-wrap">
                                <!-- RD Navbar Nav-->
                                <ul class="rd-navbar-nav">
                                    <li class="rd-nav-item active"><a class="rd-nav-link" href="index.html">Home</a>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="#">Pages</a>
                                        <!-- RD Navbar Dropdown-->
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="about-us.html">About Us</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="what-we-offer.html">What We Offer</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="our-team.html">Our Team</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="testimonials.html">Testimonials</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="pricing-list.html">Pricing List</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="grid-blog.html">Blog</a>
                                        <!-- RD Navbar Dropdown-->
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-blog.html">Grid Blog</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="blog-list.html">Blog List</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="blog-post.html">Blog Post</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="grid-gallery.html">Gallery</a>
                                        <!-- RD Navbar Dropdown-->
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-gallery.html">Grid Gallery</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="grid-fullwidth-gallery.html">Grid Fullwidth Gallery</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="masonry-gallery.html">Masonry Gallery</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="masonry-fullwidth-gallery.html">Masonry Fullwidth Gallery</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="#">Elements</a>
                                        <!-- RD Navbar Megamenu-->
                                        <ul class="rd-menu rd-navbar-megamenu">
                                            <li class="rd-megamenu-item">
                                                <div class="rd-megamenu-title"><span class="rd-megamenu-icon mdi mdi-apps"></span><span class="rd-megamenu-text">Elements</span></div>
                                                <ul class="rd-megamenu-list rd-megamenu-list-2">
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Typography</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="icon-lists.html">Icon lists</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="progress-bars.html">Progress bars</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="calls-to-action.html">Calls to action</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">Tabs &amp; accordions</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">Buttons</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tables.html">Tables</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">Forms</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="counters.html">Counters</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="grid-system.html">Grid system</a></li>
                                                </ul>
                                            </li>
                                            <li class="rd-megamenu-item flex-grow-1 flex-shrink-0">
                                                <div class="rd-megamenu-title"><span class="rd-megamenu-icon mdi mdi-layers"></span><span class="rd-megamenu-text">Additional pages</span></div>
                                                <ul class="rd-megamenu-list">
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="404-page.html">404 Page</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="coming-soon.html">Coming Soon</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="privacy-policy.html">Privacy Policy</a></li>
                                                    <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="search-results.html">Search Results</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="contact-us.html">Contacts</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        <!-- Swiper-->
        <section class="section swiper-container swiper-slider swiper-slider-4" data-loop="true" data-autoplay="4000" data-simulate-touch="false">
            <div class="swiper-wrapper context-dark">
                <div class="swiper-slide swiper-slide-1" data-slide-bg="<?= base_url('assets/images/slide-1.jpg">') ?>
                    <div class=" swiper-slide-caption section-md text-sm-start">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-md-8 col-xl-7">
                                <h1 class="swiper-title-1 text-spacing-0" data-caption-animate="fadeInLeft" data-caption-delay="100">Enjoy Scuba<br class="d-none d-xl-block"> Diving With Us</h1>
                                <h6 class="swiper-title-2 text-width-medium font-sec text-white-lighter" data-caption-animate="fadeInLeft" data-caption-delay="250">We offer a wide variety of scuba diving tours and locations for everyone.</h6>
                                <div class="button-wrap" data-caption-animate="fadeInLeft" data-caption-delay="400"><a class="button button-lg button-shadow-4 button-secondary button-zakaria" href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide swiper-slide-1" data-slide-bg="<?= base_url('assets/images/slide-2.jpg">') ?>
                    <div class=" swiper-slide-caption section-md text-sm-start">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-8 col-xl-7">
                            <h1 class="swiper-title-1" data-caption-animate="fadeInLeft" data-caption-delay="100">Best Equipment<br class="d-none d-xl-block"> and Instructors</h1>
                            <h6 class="swiper-title-2 text-width-medium font-sec text-white-lighter" data-caption-animate="fadeInLeft" data-caption-delay="250">Our team and the top-notch equipment guarantee unforgettable diving.</h6>
                            <div class="button-wrap" data-caption-animate="fadeInLeft" data-caption-delay="400"><a class="button button-lg button-shadow-4 button-secondary button-zakaria" href="#">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination"></div>
    <!-- Swiper Navigation-->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    </section>

    <!--The Tours-->
    <section class="section section-lg bg-default">
        <div class="container">
            <h3 class="wow fadeScale">Destinations</h3>
            <!-- Owl Carousel-->
            <div class="owl-carousel owl-style-8" data-items="1" data-sm-items="2" data-lg-items="3" data-margin="30" data-dots="true" data-mouse-drag="false">
                <article class="box-info-modern wow slideInUp" data-wow-delay=".1s"><a class="box-info-modern-figure" href="#"><img src="<?= base_url('assets/images/about-1-340x243.jpg') ?>" alt="" width="340" height="243" /></a>
                    <h4 class="box-info-modern-title"><a href="#">Maldives</a></h4>
                    <div class="box-info-modern-text">True abundance of marine life</div><a class="box-info-modern-link" href="#">book now</a>
                </article>
                <article class="box-info-modern wow slideInUp"><a class="box-info-modern-figure" href="#"><img src="<?= base_url('assets/images/about-2-340x243.jpg') ?>" alt="" width="340" height="243" /></a>
                    <h4 class="box-info-modern-title"><a href="#">Indonesia</a></h4>
                    <div class="box-info-modern-text">Unique marine diversity</div><a class="box-info-modern-link" href="#">book now</a>
                </article>
                <article class="box-info-modern wow slideInUp" data-wow-delay=".1s"><a class="box-info-modern-figure" href="#"><img src="<?= base_url('assets/images/about-3-340x243.jpg') ?>" alt="" width="340" height="243" /></a>
                    <h4 class="box-info-modern-title"><a href="#">Australia</a></h4>
                    <div class="box-info-modern-text">The world’s top scuba destination</div><a class="box-info-modern-link" href="#">book now</a>
                </article>
            </div>
        </div>
    </section>

    <!-- Services-->
    <section class="section section-xxl bg-image-1 bg-gray-1">
        <div class="container">
            <div class="row row-xl row-30 row-md-40 row-lg-50 align-items-center">
                <div class="col-md-5 col-xl-4">
                    <div class="row row-30 row-md-40 row-lg-50 bordered-2">
                        <div class="col-sm-6 col-md-12">
                            <article class="box-icon-classic box-icon-nancy-right text-center text-lg-right wow fadeInLeft">
                                <div class="unit flex-column flex-lg-row-reverse">
                                    <div class="unit-left">
                                        <div class="box-icon-classic-svg"><img src="<?= base_url('assets/images/index-icon-1-77x79') ?>.png" alt="" width="77" height="79" />
                                        </div>
                                    </div>
                                    <div class="unit-body">
                                        <h4 class="box-icon-classic-title text-spacing-0"><a href="#">Best Equipment</a></h4>
                                        <p class="box-icon-classic-text">Our agency provides first-class equipment for all our clients, regardless of their level.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <article class="box-icon-classic box-icon-nancy-right text-center text-lg-right wow fadeInLeft" data-wow-delay=".1s">
                                <div class="unit flex-column flex-lg-row-reverse">
                                    <div class="unit-left">
                                        <div class="box-icon-classic-svg"><img src="<?= base_url('assets/images/index-icon-2-77x79') ?>.png" alt="" width="77" height="79" />
                                        </div>
                                    </div>
                                    <div class="unit-body">
                                        <h4 class="box-icon-classic-title text-spacing-0"><a href="#">Exciting Places</a></h4>
                                        <p class="box-icon-classic-text">We offer a variety of destinations including the most exotic ones located all over the world.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xl-4 d-none d-md-block wow fadeScale"></div>
                <div class="col-md-5 col-xl-4">
                    <div class="row row-30 row-md-40 row-lg-50 bordered-2">
                        <div class="col-sm-6 col-md-12">
                            <article class="box-icon-classic box-icon-nancy-left text-center text-lg-left wow fadeInRigth">
                                <div class="unit flex-column flex-lg-row">
                                    <div class="unit-left">
                                        <div class="box-icon-classic-svg"><img src="<?= base_url('assets/images/index-icon-3-77x79') ?>.png" alt="" width="77" height="79" />
                                        </div>
                                    </div>
                                    <div class="unit-body">
                                        <h4 class="box-icon-classic-title text-spacing-0"><a href="#">100% Safety</a></h4>
                                        <p class="box-icon-classic-text">We pay a lot of attention to divers’ safety at all times, as there’s always some risk involved in it.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <article class="box-icon-classic box-icon-nancy-left text-center text-lg-left wow fadeInRight" data-wow-delay=".1s">
                                <div class="unit flex-column flex-lg-row">
                                    <div class="unit-left">
                                        <div class="box-icon-classic-svg"><img src="<?= base_url('assets/images/index-icon-4-77x79') ?>.png" alt="" width="77" height="79" />
                                        </div>
                                    </div>
                                    <div class="unit-body">
                                        <h4 class="box-icon-classic-title text-spacing-0"><a href="#">Instructors</a></h4>
                                        <p class="box-icon-classic-text">We are a team of talented and experienced diving instructors always ready to help you.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Counter Modern-->
    <section class="parallax-container" data-parallax-img="<?= base_url('assets/images/parallax-1.jpg">') ?>
            <div class=" parallax-content section-xxl context-dark">
        <div class="container">
            <div class="row row-30 justify-content-center">
                <div class="col-6 col-sm-3">
                    <div class="counter-modern">
                        <h2 class="counter-modern-number"><span class="counter">245</span>
                        </h2>
                        <div class="counter-modern-decor"></div>
                        <h6 class="counter-modern-title fw-medium font-sec">Monthly<br class="d-none d-sm-block">clients</h6>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="counter-modern">
                        <h2 class="counter-modern-number"><span class="counter">182</span>
                        </h2>
                        <div class="counter-modern-decor"></div>
                        <h6 class="counter-modern-title fw-medium font-sec">Special<br class="d-none d-sm-block"> offers</h6>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="counter-modern">
                        <h2 class="counter-modern-number"><span class="counter">1267</span>
                        </h2>
                        <div class="counter-modern-decor"></div>
                        <h6 class="counter-modern-title fw-medium font-sec">Positive<br class="d-none d-sm-block">reviews</h6>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="counter-modern">
                        <h2 class="counter-modern-number"><span class="counter">47</span>
                        </h2>
                        <div class="counter-modern-decor"></div>
                        <h6 class="counter-modern-title fw-medium font-sec">Partners throughout<br class="d-none d-sm-block"> the USA</h6>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- Why Choose Divers-->
    <section class="section section-xxl bg-default text-md-start">
        <div class="container">
            <div class="row row-30 row-md-60 row-lg-70 justify-content-center align-items-md-center">
                <div class="col-sm-8 col-md-5 col-xl-6">
                    <div class="product-wrap-1">
                        <!-- Owl Carousel-->
                        <div class="owl-carousel owl-style-5" data-items="1" data-margin="20" data-dots="true" data-autoplay="false" data-animation-out="fadeOut">
                            <article class="product-creative">
                                <div class="product-figure"><img src="<?= base_url('assets/images/product-big-1-543x558') ?>.png" alt="" width="543" height="558" />
                                </div>
                            </article>
                            <article class="product-creative">
                                <div class="product-figure"><img src="<?= base_url('assets/images/product-big-2-543x558') ?>.png" alt="" width="543" height="558" />
                                </div>
                            </article>
                            <article class="product-creative">
                                <div class="product-figure"><img src="<?= base_url('assets/images/product-big-3-543x558') ?>.png" alt="" width="543" height="558" />
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-xl-6">
                    <h3 class="text-transform-capitalize wow fadeInRight">Why Choose Divers</h3>
                    <!-- Bootstrap collapse-->
                    <div class="card-group-custom card-group-corporate" id="accordion1" role="tablist" aria-multiselectable="false">
                        <!-- Bootstrap card-->
                        <article class="card card-custom card-corporate wow fadeInRight" data-wow-delay=".1s">
                            <div class="card-header" role="tab">
                                <div class="card-title"><a id="accordion1-card-head-qcbolqpy" data-bs-toggle="collapse" data-parent="#accordion1" href="#accordion1-card-body-gexhgida" aria-controls="accordion1-card-body-gexhgida" aria-expanded="true" role="button">20+ Years of Experience in Diving
                                        <div class="card-arrow">
                                            <div class="icon"></div>
                                        </div>
                                    </a></div>
                            </div>
                            <div class="collapse show" id="accordion1-card-body-gexhgida" aria-labelledby="accordion1-card-head-qcbolqpy" data-parent="#accordion1" role="tabpanel">
                                <div class="card-body">
                                    <p>Our instructors have more than 20 years of experience in diving. It makes us the #1 choice among travelers who are looking for unforgettable and 100% safe diving experience with advice from the best divers.</p>
                                </div>
                            </div>
                        </article>
                        <!-- Bootstrap card-->
                        <article class="card card-custom card-corporate wow fadeInRight" data-wow-delay=".2s">
                            <div class="card-header" role="tab">
                                <div class="card-title"><a class="collapsed" id="accordion1-card-head-nivncuuk" data-bs-toggle="collapse" data-parent="#accordion1" href="#accordion1-card-body-ddbqmmef" aria-controls="accordion1-card-body-ddbqmmef" aria-expanded="false" role="button">Carefully Chosen Team of Instructors
                                        <div class="card-arrow">
                                            <div class="icon"></div>
                                        </div>
                                    </a></div>
                            </div>
                            <div class="collapse" id="accordion1-card-body-ddbqmmef" aria-labelledby="accordion1-card-head-nivncuuk" data-parent="#accordion1" role="tabpanel">
                                <div class="card-body">
                                    <p>Our experienced team of Diving Professionals all have over 15 years of diving experience and have been professionally certified. Our team includes the most experienced SSI instructors in the world who are always happy to help you.</p>
                                </div>
                            </div>
                        </article>
                        <!-- Bootstrap card-->
                        <article class="card card-custom card-corporate wow fadeInRight" data-wow-delay=".3s">
                            <div class="card-header" role="tab">
                                <div class="card-title"><a class="collapsed" id="accordion1-card-head-hvetuypq" data-bs-toggle="collapse" data-parent="#accordion1" href="#accordion1-card-body-pqfpojfc" aria-controls="accordion1-card-body-pqfpojfc" aria-expanded="false" role="button">Affordable Prices
                                        <div class="card-arrow">
                                            <div class="icon"></div>
                                        </div>
                                    </a></div>
                            </div>
                            <div class="collapse" id="accordion1-card-body-pqfpojfc" aria-labelledby="accordion1-card-head-hvetuypq" data-parent="#accordion1" role="tabpanel">
                                <div class="card-body">
                                    <p>Divers Center has an affordable pricing policy that allows us to offer the best diving tours and services at reasonable prices. This is one of many reasons why divers from all over US choose our center to dive & travel while also saving a lot of money.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="row row-30">
                        <div class="col-sm-6 col-lg-4 wow fadeInLeft" data-wow-delay=".2s">
                            <article class="box-icon-creative-2">
                                <div class="unit flex-column flex-md-row flex-lg-column flex-xl-row align-items-md-center align-items-lg-start align-items-xl-center">
                                    <div class="unit-left"><img src="<?= base_url('assets/images/index-icon-1-52x54') ?>.png" alt="" width="52" height="54" />
                                    </div>
                                    <div class="unit-body">
                                        <h5 class="box-icon-creative-title-2"><a href="#">Different Locations</a></h5>
                                        <p class="box-icon-creative-text-2">A wide variety of destinations</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-lg-4 wow fadeInLeft" data-wow-delay=".1s">
                            <article class="box-icon-creative-2">
                                <div class="unit flex-column flex-md-row flex-lg-column flex-xl-row align-items-md-center align-items-lg-start align-items-xl-center">
                                    <div class="unit-left"><img src="<?= base_url('assets/images/index-icon-2-52x55') ?>.png" alt="" width="52" height="55" />
                                    </div>
                                    <div class="unit-body">
                                        <h5 class="box-icon-creative-title-2"><a href="#">Free Transfer</a></h5>
                                        <p class="box-icon-creative-text-2">We offer free transfer to Divers</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-lg-4 wow fadeInLeft">
                            <article class="box-icon-creative-2">
                                <div class="unit flex-column flex-md-row flex-lg-column flex-xl-row align-items-md-center align-items-lg-start align-items-xl-center">
                                    <div class="unit-left"><img src="<?= base_url('assets/images/index-icon-3-54x54') ?>.png" alt="" width="54" height="54" />
                                    </div>
                                    <div class="unit-body">
                                        <h5 class="box-icon-creative-title-2"><a href="#">Online Payments</a></h5>
                                        <p class="box-icon-creative-text-2">Pay for any diving tour online</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Testimonials-->
    <section class="section section-xxl bg-gray-1">
        <div class="container">
            <h3 class="text-transform-capitalize wow fadeScale">Testimonials</h3>
            <div class="row row-sm row-30 justify-content-center">
                <div class="col-xl-9">
                    <div class="slick-quote">
                        <!-- Slick Carousel-->
                        <div class="slick-slider carousel-parent" id="carousel-parent" data-autoplay="true" data-swipe="true" data-items="1" data-child="#child-carousel" data-for="#child-carousel">
                            <div class="item">
                                <p class="quote-minimal-text">I had a fantastic weekend at Stoney Cove with your team. Great people who made my wife (Lisa) and I feel very welcome. Thanks for all your help and support with the diving!</p>
                            </div>
                            <div class="item">
                                <p class="quote-minimal-text">What a fantastic weekend spent with the amazingly friendly team at Divers! You made us feel very welcome and you all were amazing. Thank you!</p>
                            </div>
                            <div class="item">
                                <p class="quote-minimal-text">It was a great first time dive, the team was very friendly and attentive I will have no hesitation going again and recommending you to all my friends and colleagues.</p>
                            </div>
                        </div>
                        <div class="slick-quote-nav">
                            <div class="slick-slider child-carousel" id="child-carousel" data-arrows="true" data-for="#carousel-parent" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="3" data-xl-items="3" data-xxl-items="3">
                                <div class="item">
                                    <div class="quote-minimal-figure"><img src="<?= base_url('assets/images/user-7-87x87.jpg') ?>" alt="" width="87" height="87" />
                                    </div>
                                    <div class="quote-minimal-author">Patrick Goodman</div>
                                    <div class="quote-minimal-status">Client</div>
                                </div>
                                <div class="item">
                                    <div class="quote-minimal-figure"><img src="<?= base_url('assets/images/user-8-87x87.jpg') ?>" alt="" width="87" height="87" />
                                    </div>
                                    <div class="quote-minimal-author">Kate Smith</div>
                                    <div class="quote-minimal-status">Client</div>
                                </div>
                                <div class="item">
                                    <div class="quote-minimal-figure"><img src="<?= base_url('assets/images/user-9-87x87.jpg') ?>" alt="" width="87" height="87" />
                                    </div>
                                    <div class="quote-minimal-author">Sam Wilson</div>
                                    <div class="quote-minimal-status">Client</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Masonry Gallery-->
    <section class="section section-xxl bg-default">
        <div class="container">
            <h3 class="text-transform-capitalize wow fadeScale">Gallery</h3>
            <div class="isotope-wrap">
                <div class="isotope-filters"></div>
                <div class="row row-30 isotope isotope-custom-1" data-lightgallery="group">
                    <div class="col-sm-6 col-lg-4 col-xl-3 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-1-270x250') ?>.jpg" alt="" width="270" height="250" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Barracuda Point</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/grid-gallery-2-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-1-270x250') ?>.jpg" alt="" width="270" height="250" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-2-270x530') ?>.jpg" alt="" width="270" height="530" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Blue Corner Wall</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/masonry-gallery-2-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-2-270x530') ?>.jpg" alt="" width="270" height="530" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-3-270x250') ?>.jpg" alt="" width="270" height="250" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">The Yongala</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/masonry-gallery-3-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-3-270x250') ?>.jpg" alt="" width="270" height="250" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-4-270x250') ?>.jpg" alt="" width="270" height="250" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Thistlegorm</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/masonry-gallery-4-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-4-270x250') ?>.jpg" alt="" width="270" height="250" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-5-270x250') ?>.jpg" alt="" width="270" height="250" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Shark and Yolanda Reef</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/grid-gallery-3-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-5-270x250') ?>.jpg" alt="" width="270" height="250" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-6 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-6-570x530') ?>.jpg" alt="" width="570" height="530" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Manta Ray Night Dive</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/grid-fullwidth-gallery-2') ?>-1200x800-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-6-570x530') ?>.jpg" alt="" width="570" height="530" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-6 col-lg-8 col-xl-6 isotope-item">
                        <!-- Thumbnail Classic-->
                        <article class="thumbnail-classic">
                            <div class="thumbnail-classic-figure"><img src="<?= base_url('assets/images/masonry-gallery-7-570x250') ?>.jpg" alt="" width="570" height="250" />
                            </div>
                            <div class="thumbnail-classic-caption">
                                <div>
                                    <h5 class="thumbnail-classic-title"><a href="#">Great Blue Hole</a></h5>
                                    <div class="thumbnail-classic-button-wrap">
                                        <div class="thumbnail-classic-button"><a class="button button-primary-2 button-zakaria fl-bigmug-line-search74" href="<?= base_url('assets/images/masonry-gallery-7-1200x800') ?>-original.jpg" data-lightgallery="item"><img src="<?= base_url('assets/images/masonry-gallery-7-570x250') ?>.jpg" alt="" width="570" height="250" /></a></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Subscribe to Our Newsletter-->
    <section class="parallax-container" data-parallax-img="<?= base_url('assets/images/parallax-4.jpg">') ?>
            <div class=" parallax-content section-xxl context-dark text-md-left">
        <div class="container">
            <div class="row row-30 justify-content-center align-items-center align-items-md-end">
                <div class="col-lg-3">
                    <h3 class="text-spacing-0 wow fadeInLeft">Newsletter</h3>
                    <p class="text-opacity-70 wow fadeInLeft" data-wow-delay=".1s">Subscribe to our newsletter</p>
                </div>
                <div class="col-lg-9 inset-lg-bottom-10">
                    <!-- RD Mailform-->
                    <form class="rd-form rd-mailform rd-form-inline form-lg rd-form-text-center" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                        <div class="form-wrap wow fadeInUp">
                            <input class="form-input" id="subscribe-form-0-email" type="email" name="email" data-constraints="@Email @Required" />
                            <label class="form-label" for="subscribe-form-0-email">Enter your e-mail address</label>
                        </div>
                        <div class="form-button wow fadeInRight">
                            <button class="button button-shadow-2 button-zakaria button-lg button-secondary" type="submit">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- Instructors-->
    <section class="section section-xxl bg-default">
        <div class="container">
            <h3 class="text-spacing-0 text-transform-capitalize wow fadeScale">Instructors</h3>
            <!-- Owl Carousel-->
            <div class="owl-carousel owl-style-9" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="4" data-margin="30" data-dots="true" data-mouse-drag="false">
                <article class="team-modern box-sm wow slideInUp"><a class="team-modern-figure" href="#"><img src="<?= base_url('assets/images/team-4-270x227.jpg') ?>" alt="" width="270" height="227" /></a>
                    <h5 class="team-modern-name"><a href="#">Peter Martinez</a></h5>
                    <p class="team-modern-text">Peter is PADI Course Director and our company’s Founder</p>
                    <ul class="list-inline team-modern-list-social list-social-2 list-inline-sm">
                        <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                        <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                        <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                        <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                </article>
                <article class="team-modern box-sm wow slideInUp" data-wow-delay=".1s"><a class="team-modern-figure" href="#"><img src="<?= base_url('assets/images/team-5-270x227.jpg') ?>" alt="" width="270" height="227" /></a>
                    <h5 class="team-modern-name"><a href="#">Jane McMillan</a></h5>
                    <p class="team-modern-text">Jane is Divers’s Specialty Instructor Trainer who joined us in 2010.</p>
                    <ul class="list-inline team-modern-list-social list-social-2 list-inline-sm">
                        <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                        <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                        <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                        <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                </article>
                <article class="team-modern box-sm wow slideInUp" data-wow-delay=".2s"><a class="team-modern-figure" href="#"><img src="<?= base_url('assets/images/team-6-270x227.jpg') ?>" alt="" width="270" height="227" /></a>
                    <h5 class="team-modern-name"><a href="#">John Smith</a></h5>
                    <p class="team-modern-text">John has been working with us since day one as PADI Divemaster.</p>
                    <ul class="list-inline team-modern-list-social list-social-2 list-inline-sm">
                        <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                        <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                        <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                        <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                </article>
                <article class="team-modern box-sm wow slideInUp" data-wow-delay=".3s"><a class="team-modern-figure" href="#"><img src="<?= base_url('assets/images/team-7-270x227.jpg') ?>" alt="" width="270" height="227" /></a>
                    <h5 class="team-modern-name"><a href="#">Sam Williams</a></h5>
                    <p class="team-modern-text">Sam is Divers’s PADI Open Water Scuba Instructor.</p>
                    <ul class="list-inline team-modern-list-social list-social-2 list-inline-sm">
                        <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                        <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                        <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                        <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                </article>
            </div>
        </div>
    </section>

    <!-- Blog Post-->
    <section class="section section-xxl bg-gray-1">
        <div class="container">
            <h3 class="text-transform-capitalize text-spacing-0 wow fadeScale">Our Blog</h3>
            <!-- Owl Carousel-->
            <div class="owl-carousel" data-items="1" data-sm-items="2" data-lg-items="3" data-margin="30" data-dots="true" data-mouse-drag="false">
                <!-- Post Classic-->
                <article class="post post-classic box-md wow slideInDown"><a class="post-classic-figure" href="blog-post.html"><img src="<?= base_url('assets/images/post-1-370x239.jpg') ?>" alt="" width="370" height="239" /></a>
                    <div class="post-classic-content">
                        <div class="post-classic-time">
                            <time datetime="2021-08-09">August 9, 2021</time>
                        </div>
                        <h5 class="post-classic-title text-spacing-0"><a href="blog-post.html">New Caledonia: The Lagoon of All Hopes</a></h5>
                        <p class="post-classic-text font-fourth">As we dived for the very first time in the lagoon of New Caledonia, at Duck Island, with only our masks and fins...</p>
                    </div>
                </article>
                <!-- Post Classic-->
                <article class="post post-classic box-md wow slideInUp"><a class="post-classic-figure" href="blog-post.html"><img src="<?= base_url('assets/images/post-2-370x239.jpg') ?>" alt="" width="370" height="239" /></a>
                    <div class="post-classic-content">
                        <div class="post-classic-time">
                            <time datetime="2021-08-09">August 9, 2021</time>
                        </div>
                        <h5 class="post-classic-title text-spacing-0"><a href="blog-post.html">Scuba Diving in Okinawa: A First-timer Guide</a></h5>
                        <p class="post-classic-text font-fourth">It is one of these places where it is not so straightforward to organize a scuba diving trip. There are so many...</p>
                    </div>
                </article>
                <!-- Post Classic-->
                <article class="post post-classic box-md wow slideInDown"><a class="post-classic-figure" href="blog-post.html"><img src="<?= base_url('assets/images/post-3-370x239.jpg') ?>" alt="" width="370" height="239" /></a>
                    <div class="post-classic-content">
                        <div class="post-classic-time">
                            <time datetime="2021-08-09">August 9, 2021</time>
                        </div>
                        <h5 class="post-classic-title text-spacing-0"><a href="blog-post.html">Shore Diving in Maui &amp; Big Island</a></h5>
                        <p class="post-classic-text font-fourth">The Hawaiian Archipelago is full of bucket list dives with exciting adventures such as diving in the crater...</p>
                    </div>
                </article>
            </div>
        </div>
    </section>